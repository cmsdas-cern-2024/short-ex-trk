# short-ex-trk
Code for the CMSDAS Schools Short Tracking Exercise

## General directions:
The relevant links for this short exercise are:
* TWiki page: [SWGuideCMSDataAnalysisSchoolCERN2024TrackingVertexingShortExercise](https://twiki.cern.ch/twiki/bin/view/CMS/SWGuideCMSDataAnalysisSchoolCERN2024TrackingVertexingShortExercise).
* Alternate version of the Twiki page: [https://trackingvertexing-cmsdas-2024.docs.cern.ch/](https://trackingvertexing-cmsdas-2024.docs.cern.ch/)
* Introductory slides: [link](https://twiki.cern.ch/twiki/pub/CMS/SWGuideCMSDataAnalysisSchoolCERN2024TrackingVertexingShortExercise/CMSDASCERN2024_TrackingVertexingExercise_Introduction.pdf).
* Wrap-up slides: [link](https://twiki.cern.ch/twiki/pub/CMS/SWGuideCMSDataAnalysisSchoolCERN2024TrackingVertexingShortExercise/CMSDASCERN2024_TrackingVertexingExercise_Wrapup.pdf).
* Any questions? Use the [mattermost channel](https://mattermost.web.cern.ch/cmsdas24/channels/short-ex-trk)!

## The setup
This package is meant to provide solutions to the exercises 

* Setup in `CMSSW_14_0_9` 

```
scram p -n cmssw CMSSW_14_0_9
cd cmssw/src/
cmsenv
git clone https://gitlab.cern.ch/cmsdas-cern-2024/short-ex-trk.git -b master .
scram b -j 8
```

Then simply run the configs
```
cmsRun MyDirectory/PrintOutTracks/test/run_cfg.py
cmsRun MyDirectory/PrintOutTracks/test/run_cfg_MVA.py
cmsRun MyDirectory/PrintOutTracks/test/construct_secondary_vertices_cfg.py
```

or the scripts in 

`$CMSSW_BASE/src/MyDirectory/PrintOutTracks/scripts/`